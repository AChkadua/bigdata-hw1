package ru.mephi.bigdata.lettercount;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class LetterCountTest {
    private Configuration conf;
    private Path input;
    private Path output;

    @Before
    public void setup() throws IOException {
        conf = new Configuration();
        conf.set("fs.default.name", "file:///");
        conf.set("mapred.job.tracker", "local");
        input = new Path("src/test/resources/input");
        output = new Path("target/output/");
        FileSystem fs = FileSystem.getLocal(conf);
        fs.delete(output, true);
    }

    @Test
    public void test() throws Exception {
        LetterCount letterCount = new LetterCount();
        letterCount.setConf(conf);
        int exitCode = letterCount.run(new String[] {input.toString(), output.toString()});
        assertEquals(0, exitCode);

        try (SequenceFile.Reader reader = new SequenceFile.Reader(FileSystem.get(conf),
                new Path("target/output/part-r-00000"), conf)) {
            IntWritable key = IntWritable.class.newInstance();
            Text value = Text.class.newInstance();
            reader.next(key, value);
            assertEquals("6", key.toString());
            assertEquals("qwerty", value.toString());
        }
    }
}
