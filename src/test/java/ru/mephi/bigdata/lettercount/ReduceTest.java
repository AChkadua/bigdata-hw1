package ru.mephi.bigdata.lettercount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReduceTest {

    private LetterCountReducer reducer = new LetterCountReducer();

    @Before
    public void setup() {
        reducer.setMax(6);
    }

    @Test
    public void testReduce() throws Exception {
        Context context = mock(Context.class);
        reducer.reduce(new IntWritable(6), Collections.singletonList(new Text("qwerty")), context);
        reducer.reduce(new IntWritable(5), Collections.singletonList(new Text("asdfg")), context);
        reducer.reduce(new IntWritable(4), Collections.singletonList(new Text("zxcv")), context);
        verify(context).write(eq(new IntWritable(6)), eq(new Text("qwerty")));
        verifyNoMoreInteractions(context);
    }
}
