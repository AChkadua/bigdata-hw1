package ru.mephi.bigdata.lettercount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MapperTest {

    private LetterCountMapper mapper = new LetterCountMapper();

    @Test
    public void testMapOnValidData() throws Exception {
        Context context = mock(Context.class);
        mapper.map(new LongWritable(1), new Text("qwerty"), context);
        mapper.map(new LongWritable(2), new Text("asdfg"), context);
        mapper.map(new LongWritable(3), new Text("zxcv"), context);
        verify(context).write(eq(new IntWritable(6)), eq(new Text("qwerty")));
        verify(context).write(eq(new IntWritable(5)), eq(new Text("asdfg")));
        verify(context).write(eq(new IntWritable(4)), eq(new Text("zxcv")));
        verifyNoMoreInteractions(context);
    }

    @Test
    public void testMapOnInvalidData() throws Exception {
        Context context = mock(Context.class);
        Counter counter = mock(Counter.class);
        doNothing().when(counter).increment(anyLong());
        when(context.getCounter(any(Enum.class))).thenReturn(counter);
        mapper.map(new LongWritable(1), new Text("qwertπ"), context);
        mapper.map(new LongWritable(2), new Text("qwerty"), context);
        mapper.map(new LongWritable(3), new Text("qwert±"), context);
        verify(context).write(eq(new IntWritable(6)), eq(new Text("qwerty")));
        verify(context, times(2)).getCounter(any(Enum.class));
        verify(counter, times(2)).increment(eq(1L));
        verifyNoMoreInteractions(context, counter);
    }
}
