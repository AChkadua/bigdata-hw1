package ru.mephi.bigdata.lettercount;

import com.google.common.collect.Sets;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Set;

/**
 * Reducer for words lengths.
 * Input: ("" -> words lengths)
 * Output: (words lengths, unique -> "")
 */
public class WordLengthReducer extends Reducer<Text, IntWritable, IntWritable, Text> {

    private static final Text EMPTY = new Text("");

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        Set<IntWritable> lengthsSet = Sets.newHashSet(values);
        for (IntWritable length : lengthsSet) {
            context.write(length, EMPTY);
        }
    }
}
