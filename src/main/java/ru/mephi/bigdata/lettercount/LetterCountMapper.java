package ru.mephi.bigdata.lettercount;

import com.google.common.base.CharMatcher;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Mapper for counting length of words.
 * Input: tuples (current byte -> line), each line is a single word
 * Output: tuples (word length -> word)
 */
public class LetterCountMapper extends Mapper<LongWritable, Text, IntWritable, Text> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();

        if (!CharMatcher.ASCII.matchesAllOf(line)) {
            context.getCounter(LetterCountCounters.INVALID_STRINGS).increment(1);
        } else {
            String[] words = line.trim().split("\\s");
            for (String word : words) {
                context.write(new IntWritable(word.length()), new Text(word));
            }
        }
    }
}
