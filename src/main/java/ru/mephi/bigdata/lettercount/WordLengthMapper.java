package ru.mephi.bigdata.lettercount;

import com.google.common.base.CharMatcher;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Counts lengths of words.
 * Input: (bytes read -> line)
 * Output: ("" -> word length)
 */
public class WordLengthMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private Text EMPTY = new Text("");

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();

        if (CharMatcher.ASCII.matchesAllOf(line)) {
            String[] words = line.trim().split("\\s");
            for (String word : words) {
                context.write(EMPTY, new IntWritable(word.length()));
            }
        }
    }
}
