package ru.mephi.bigdata.lettercount;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Searches for max length and words having it in the text file and outputs both to the sequence file.
 * Input - file with every word on a separate line.
 * Output - file with
 */
public class LetterCount extends Configured implements Tool {

    public static final String TMP_FILES_DIRECTORY = "/tmp/_setup";

    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new LetterCount(), args));
    }

    @Override
    public int run(String[] strings) throws Exception {
        if (strings.length != 2) {
            System.err.println("WRONG USAGE");
            return -1;
        }

        // Counting the lengths of all the words in file to find the max word length later
        Job setupJob = Job.getInstance();
        setupJob.setJarByClass(LetterCount.class);
        setupJob.setJobName("Counting lengths of all words");
        FileInputFormat.addInputPath(setupJob, new Path(strings[0]));
        FileOutputFormat.setOutputPath(setupJob, new Path(TMP_FILES_DIRECTORY));

        setupJob.setMapOutputKeyClass(Text.class);
        setupJob.setMapOutputValueClass(IntWritable.class);
        setupJob.setOutputKeyClass(IntWritable.class);
        setupJob.setOutputValueClass(Text.class);
        setupJob.setOutputFormatClass(TextOutputFormat.class);

        setupJob.setMapperClass(WordLengthMapper.class);
        setupJob.setReducerClass(WordLengthReducer.class);

        setupJob.waitForCompletion(true);
        if (!setupJob.isSuccessful()) {
            System.err.println("Error while running setup job, aborting...");
            FileSystem hdfs = FileSystem.get(setupJob.getConfiguration());
            hdfs.delete(new Path(TMP_FILES_DIRECTORY), true);
            return 1;
        }

        // Search for words with max length
        Job job = Job.getInstance();
        job.setJarByClass(LetterCount.class);
        job.setJobName("Letter count");
        job.addCacheFile(new Path(TMP_FILES_DIRECTORY + "/part-r-00000").toUri());

        FileInputFormat.addInputPath(job, new Path(strings[0]));
        FileOutputFormat.setOutputPath(job, new Path(strings[1]));

        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        job.setMapperClass(LetterCountMapper.class);
        job.setReducerClass(LetterCountReducer.class);

        int returnValue = job.waitForCompletion(true) ? 0 : 1;

        FileSystem hdfs = FileSystem.get(job.getConfiguration());
        hdfs.delete(new Path(TMP_FILES_DIRECTORY), true);
        System.err.println(job.isSuccessful() ? "SUCCESS" : "FAIL");
        return returnValue;
    }
}
