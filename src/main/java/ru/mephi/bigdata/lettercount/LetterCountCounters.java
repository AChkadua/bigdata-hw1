package ru.mephi.bigdata.lettercount;

/**
 * User defined counters
 */
public enum LetterCountCounters {
    /**
     * Counts invalid strings (strings that contain non-ASCII chars)
     */
    INVALID_STRINGS
}
