package ru.mephi.bigdata.lettercount;

import com.google.common.annotations.VisibleForTesting;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URI;

/**
 * Writes words with max length
 * Input: tuple (length -> words with this length)
 * Output: a single tuple (max length -> words with this length)
 */
public class LetterCountReducer extends Reducer<IntWritable, Text, IntWritable, Text> {

    private int max = 0;

    @VisibleForTesting
    void setMax(int max) {
        this.max = max;
    }

    @Override
    protected void setup(Context context) throws IOException {
        URI wordsLengthsFile = context.getCacheFiles()[0];
        Path path = new Path(wordsLengthsFile);
        try (LineNumberReader reader = new LineNumberReader(new FileReader(path.getName()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                int buf = Integer.parseInt(line.trim());
                if (buf > max) {
                    max = buf;
                }
            }
        }
    }

    @Override
    protected void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        if (key.get() == max) {
            for (Text value : values) {
                context.write(key, value);
            }
        }
    }
}
