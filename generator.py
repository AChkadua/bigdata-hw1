# coding=utf-8
import string
import sys
import os.path
import random

if len(sys.argv) != 4:
    print("""Usage: python3 generator.py <dest> <lines> <max_chars>
    dest - output file or stdout
    lines - number of lines to generate
    max_chars - max amount of chars in word""")
    exit(-1)

if sys.argv[1] == "stdout":
    file = sys.stdout
else:
    if os.path.isfile(sys.argv[1]):
        print(f"File {sys.argv[1]} exists, overwrite? (y/N)")
        overwrite = input().lower()
        while (overwrite != "y") & (overwrite != "n") & (overwrite != ""):
            print("Input \"y\" or \"n\" (case-insensitive)")
            overwrite = input().lower()
        if (overwrite == "n") | (overwrite == ""):
            print("File will not be overwritten, exiting")
            exit(0)
        else:
            print(f"Overwriting file {sys.argv[1]}")
            file = open(sys.argv[1], "w")
    else:
        file = open(sys.argv[1], "w")

for _ in range(int(sys.argv[2])):
    line = ""
    numWords = random.randint(1, 10)
    for _ in range(0, numWords):
        length = random.randint(1, int(sys.argv[3]))
        for _ in range(0, length):
            isAscii = random.randint(1, 100)
            if isAscii < 97:
                line = line + random.choice(string.ascii_letters)
            else:
                line = line + random.choice("猫文字化けΩḍ̇Åℍ")
        line += " "
    file.write(line + "\n")
