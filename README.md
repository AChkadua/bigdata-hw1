# Letter count

## Requirements

* Linux
* Python 3
* Maven
* Hadoop 2.7.2
* Java 7
* 4+ GB RAM

## Setup

1. Clone repo
2. Run `mvn package` to build fat JAR
3. Run `python3 generator.py <destination> <num_lines> <max_chars_in_word>` to generate input files
4. Copy input files to HDFS: `hadoop fs -copyFromLocal <source> <destination>`
5. Run the JAR: `yarn jar <path_to_jar> <input_file> <output_destination>`

